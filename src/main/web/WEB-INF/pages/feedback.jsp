<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 04.05.2021
  Time: 17:13
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp"/>
<h2>in feedback page</h2>

<nav><a href="${pageContext.request.contextPath}/feedbacks/action/add">Add Feedback</a>
</nav>
<c:forEach var="feedback" items="${feedbacks}">
    <ul>
        <li> Description: <c:out value="${feedback.description}"/></li>
        <li> Create Date: <c:out value="${feedback.createDate}"/></li>
            <form name="delete_feedback_Form" accept-charset="UTF-8" method="POST"
                  action="${pageContext.request.contextPath}/feedbacks/${feedback.id}">
                <input type="submit" value="Delete"/>
            </form>
    </ul>
    <hr/>
</c:forEach>
</body>
</html>
