<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 05.05.2021
  Time: 12:08
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp" />
<h2>in project page</h2>

<nav><a href="${pageContext.request.contextPath}/projects/action/add">Add Project</a>
</nav>
<c:forEach var="project" items="${projects}">
    <ul>
        <li> Name: <c:out value="${project.name}"/></li>
        <li> Customer: <c:out value="${project.customer}"/></li>
        <li> Duration: <c:out value="${project.duration}"/></li>
        <li> Methodology: <c:out value="${project.methodology}"/></li>
        <li> Manager Id: <c:out value="${project.projectManagerId}"/></li>
        <li> Team Id: <c:out value="${project.teamId}"/></li>
            <form name="delete_project_Form" accept-charset="UTF-8" method="POST"
                  action="${pageContext.request.contextPath}/projects/${project.id}">
                <input type="submit" value="Delete"/>
            </form>
    </ul>
    <hr/>
</c:forEach>

</body>
</html>
