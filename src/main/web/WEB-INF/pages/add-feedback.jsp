<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 06.05.2021
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp"/>
<h2>Add Feedback</h2>
<form name="add_project_Form" modelAttribute="feedback" accept-charset="UTF-8" method="POST"
      action="${pageContext.request.contextPath}/feedbacks">
    <table>
        <tr>
            <td><label for="description"> Description</label></td>
            <td> <textarea class="form-control" name="description" accept-charset="UTF-8" id="description"
                           placeholder="Add description less 400 symbols"> </textarea></td>
        </tr>

        <tr>
            <td><input type="submit" value="Add Feedback"/></td>
        </tr>
    </table>
</form>

</body>
</html>
