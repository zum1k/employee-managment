<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 06.05.2021
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp"/>
<h2>Add Team</h2>
<%--@elvariable id="team" type="com.andersenlab.trainee.entity.Team"--%>
<form:form method = "POST" modelAttribute="team"  action = "/employee_management/teams">
    <table>
        <tr>
            <td><form:label path = "name">Name</form:label></td>
            <td><form:input path = "name" /></td>
        </tr>
        <tr>
            <td colspan = "2">
                <input type = "submit" value = "Submit"/>
            </td>
        </tr>
    </table>
</form:form>

</body>
</html>
