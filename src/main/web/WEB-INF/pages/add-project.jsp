<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 06.05.2021
  Time: 19:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp"/>
<h2>Add Project</h2>
<form name="add_project_Form" modelAttribute="project" accept-charset="UTF-8" method="POST"
      action="${pageContext.request.contextPath}/projects">
    <table>
        <tr>
            <div class="form-group">
                <td><label for="name">Name </label></td>
                <td><input type="text" pattern="^[a-zA-Zа-яА-Я]+(?:[\s-][a-zA-Zа-яА-Я]+)*$" class="form-control"
                           required id="name"
                           name="name" placeholder="name"
                           value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="customer">Customer </label></td>
                <td><input type="text" pattern="^[a-zA-Zа-яА-Я]+(?:[\s-][a-zA-Zа-яА-Я]+)*$" class="form-control"
                           required
                           id="customer" name="customer" placeholder="customer"
                           value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="duration">Duration </label></td>
                <td><input type="text" class="form-control" required id="duration" name="duration"
                           placeholder="xx" min="1" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="methodology">Methodology</label></td>
                <td> <textarea class="form-control" name="methodology" accept-charset="UTF-8" id="methodology"
                               placeholder="Add methodology less 150 symbols"> </textarea></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="projectManagerId">Manager ID</label></td>
                <td><input type="text" class="form-control" required id="projectManagerId" name="projectManagerId"
                           placeholder="xx" min="1" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="teamId">Team ID</label></td>
                <td><input type="text" class="form-control" required id="teamId" name="teamId"
                           placeholder="xx" min="1" value=""></td>
            </div>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Add Project"/></td>
        </tr>
    </table>

</form>

</body>
</html>
