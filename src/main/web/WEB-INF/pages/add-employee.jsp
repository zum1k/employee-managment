<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 06.05.2021
  Time: 19:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp"/>
<h2>Add Employee</h2>

<form name="add_employee_form" modelAttribute="employee" accept-charset="UTF-8" method="POST"
      action="${pageContext.request.contextPath}/employees">
    <table>
        <tr>
            <div class="form-group">
                <td><label for="name">Name </label></td>
                <td><input type="text" pattern="^[a-zA-Zа-яА-Я]+(?:[\s-][a-zA-Zа-яА-Я]+)*$" class="form-control"
                           required id="name"
                           name="name" placeholder="name"
                           value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="surname">Surname </label></td>
                <td><input type="text" pattern="^[a-zA-Zа-яА-Я]+(?:[\s-][a-zA-Zа-яА-Я]+)*$" class="form-control"
                           required
                           id="surname" name="surname" placeholder="surname"
                           value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="patronymic"> Patronymic</label></td>
                <td><input type="text" pattern="^[a-zA-Zа-яА-Я]+(?:[\s-][a-zA-Zа-яА-Я]+)*$" class="form-control"
                           required
                           id="patronymic" name="patronymic" placeholder="patronymic"
                           value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="phoneNumber"> Phone Number </label></td>
                <td><input type="text" pattern="^(\+375)(29|25|44|33|17)(\d{3})(\d{2})(\d{2})$" class="form-control"
                           id="phoneNumber" required name="phoneNumber"
                           placeholder="+375xxxxxxxxx" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="birthday">Birthday </label></td>
                <td><input type="text" class="form-control" required id="birthday" name="birthday"
                           placeholder="1900-01-01" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="dateOfEmployment">Date of Employment </label></td>
                <td><input type="text" class="form-control" required id="dateOfEmployment" name="dateOfEmployment"
                           placeholder="1900-01-01" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="projectId">Project ID </label></td>
                <td><input type="text" class="form-control" required id="projectId" name="projectId"
                           placeholder="xx" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="developerLevel">Developer Level </label></td>
                <td><select class="form-control" id="developerLevel" name="developerLevel">
                    <option value="J1">J1</option>
                    <option value="J2">J2</option>
                    <option value="M1">M1</option>
                    <option value="M2">M2</option>
                    <option value="M3">M3</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                </select></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="englishLevel">English Level </label></td>
                <td><select class="form-control" id="englishLevel" name="englishLevel">
                    <option value="A1">A1</option>
                    <option value="A2">A2</option>
                    <option value="B1">B1</option>
                    <option value="B2">B2</option>
                    <option value="C1">C1</option>
                    <option value="C2">C2</option>
                </select></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="skype">Skype </label></td>
                <td><input type="text" class="form-control" required id="skype" name="skype"
                           placeholder="skype login" value=""></td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td><label for="feedbackId">Feedback ID </label></td>
                <td><input type="text" class="form-control" required id="feedbackId" name="feedbackId"
                           placeholder="xx" value=""></td>
            </div>
        </tr>
        <tr>
            <td><input type="submit" value="Add Employee"/></td>
        </tr>
    </table>
</form>
</body>
</html>
