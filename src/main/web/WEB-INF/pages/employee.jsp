<%--
  Created by IntelliJ IDEA.
  User: smugl
  Date: 05.05.2021
  Time: 00:08
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/WEB-INF/pages/header.jsp"/>
<h2>in employee page</h2>

<nav> <a href="${pageContext.request.contextPath}/employees/action/add">Add Employee</a>
</nav>
<c:forEach var="employee" items="${employees}">
    <ul>
        <li> Name: <c:out value="${employee.name} ${employee.surname} ${employee.patronymic}"/></li>
        <li> Phone number: <c:out value="${employee.phoneNumber}"/></li>
        <li> Birthday: <c:out value="${employee.birthday}"/></li>
        <li> Developer level: <c:out value="${employee.developerLevel}"/></li>
        <li> English level: <c:out value="${employee.englishLevel}"/></li>
        <li> Skype: <c:out value="${employee.skype}"/></li>
            <form name="delete_employee_Form" accept-charset="UTF-8" method="POST"
                  action="${pageContext.request.contextPath}/employees/${employee.id}">
                <input type="submit" value="Delete"/>
            </form>
    </ul>
    <hr/>
</c:forEach>

</body>
</html>
