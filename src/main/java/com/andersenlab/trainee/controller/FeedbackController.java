package com.andersenlab.trainee.controller;

import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.service.FeedbackService;
import com.andersenlab.trainee.utils.ModelAndViewConstant;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(value = "/feedbacks")
public class FeedbackController {
    private static final Logger logger = LogManager.getLogger(FeedbackController.class);
    private final FeedbackService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ModelAndView add(@ModelAttribute("feedback") Feedback feedback) {
        logger.info("add feedback");
        feedback.setCreateDate(LocalDate.now());
        service.add(feedback);
        return findAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView findAll() {
        logger.info("find all Feedbacks");
        List<Feedback> feedbacks = service.findAll();
        ModelAndView map = new ModelAndView("feedback");
        map.addObject(ModelAndViewConstant.FEEDBACKS, feedbacks);
        return map;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Feedback findById(@PathVariable("id") final long id) {
        logger.info("find feedback by id = {}", id);
        return service.findById(id).get();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView deleteById(@PathVariable("id") final long id) {
        logger.info("delete feedback by id = {}", id);
        service.remove(id);
        return findAll();
    }

    @RequestMapping(value = "/action/add", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView linkToAdd() {
        logger.info("link to add feedback");
        Feedback feedback = new Feedback();
        ModelAndView modelAndView = new ModelAndView("add-feedback");
        modelAndView.addObject(ModelAndViewConstant.FEEDBACK, feedback);
        return modelAndView;
    }

}
