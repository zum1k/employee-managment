package com.andersenlab.trainee.controller;

import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.service.ProjectService;
import com.andersenlab.trainee.utils.ModelAndViewConstant;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(value = "/projects")
public class ProjectController {
    private static final Logger logger = LogManager.getLogger(ProjectController.class);
    private final ProjectService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ModelAndView add(@ModelAttribute("project") Project project) {
        logger.info("add project");
        service.add(project);
        return new ModelAndView("project");
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView findAll() {
        logger.info("find all Projects");
        List<Project> projects = service.findAll();
        ModelAndView map = new ModelAndView("project");
        map.addObject(ModelAndViewConstant.PROJECTS, projects);
        return map;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Project findById(@PathVariable("id") final long id) {
        logger.info("find project by id = {}", id);
        return service.findById(id).get();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView deleteById(@PathVariable("id") final long id) {
        logger.info("delete project by id = {}", id);
        service.remove(id);
        return findAll();
    }

    @RequestMapping(value = "/action/add", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView linkToAdd() {
        logger.info("link to add project");
        Project project = new Project();
        ModelAndView map = new ModelAndView("add-project");
        map.addObject(ModelAndViewConstant.PROJECT, project);
        return map;
    }
}
