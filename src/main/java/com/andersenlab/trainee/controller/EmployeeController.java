package com.andersenlab.trainee.controller;

import com.andersenlab.trainee.entity.Employee;
import com.andersenlab.trainee.service.EmployeeService;
import com.andersenlab.trainee.utils.ModelAndViewConstant;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(value = "/employees")
public class EmployeeController {
    private static final Logger logger = LogManager.getLogger(EmployeeController.class);
    private final EmployeeService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ModelAndView add(@ModelAttribute("employee") Employee employee) {
        logger.info("add employee");
        service.add(employee);
        return new ModelAndView("employee");
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView findAll() {
        logger.info("find all Employees");
        List<Employee> employees = service.findAll();
        ModelAndView map = new ModelAndView("employee");
        map.addObject(ModelAndViewConstant.EMPLOYEES, employees);
        return map;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Employee findById(@PathVariable("id") final long id) {
        logger.info("find employee by id = {}", id);
        return service.findById(id).get(); //TODO
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView deleteById(@PathVariable("id") final long id) {
        logger.info("delete employee by id = {}", id);
        service.remove(id);
        return findAll();

    }

    @RequestMapping(value = "/action/add", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView linkToAdd() {
        logger.info("link to add employee");
        Employee employee = new Employee();
        ModelAndView modelAndView = new ModelAndView("add-employee");
        modelAndView.addObject(ModelAndViewConstant.EMPLOYEE, employee);
        return modelAndView;
    }

    @RequestMapping(value = "/action/update", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView linkToUpdate() {
        logger.info("link to add employee");
        return new ModelAndView("add-employee");
    }
}
