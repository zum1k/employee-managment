package com.andersenlab.trainee.controller;

import com.andersenlab.trainee.entity.Team;
import com.andersenlab.trainee.service.TeamService;
import com.andersenlab.trainee.utils.ModelAndViewConstant;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(value = "/teams")
public class TeamController {
    private static final Logger logger = LogManager.getLogger(TeamController.class);
    private final TeamService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ModelAndView add(@ModelAttribute("team") Team team) {
        logger.info("add team");
        service.add(team);
        return findAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView findAll() {
        logger.info("find all teams");
        List<Team> teams = service.findAll();
        ModelAndView map = new ModelAndView("team");
        map.addObject(ModelAndViewConstant.TEAMS, teams);
        return map;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView findById(@PathVariable("id") final long id) {
        logger.info("find team by id = {}", id);
        Team team = service.findById(id).get();
        return findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView deleteById(@PathVariable("id") final long id) {
        logger.info("delete team by id = {}", id);
        service.remove(id);
        return findAll();
    }

    @RequestMapping(value = "/action/add", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView linkToAdd() {
        logger.info("link to add team");
        Team team = new Team();
        ModelAndView map = new ModelAndView("add-team");
        map.addObject(ModelAndViewConstant.TEAM, team);
        return map;
    }
}
