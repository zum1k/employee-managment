package com.andersenlab.trainee.utils;

public class ModelAndViewConstant {
    public static String EMPLOYEE = "employee";
    public static String EMPLOYEES = "employees";
    public static String PROJECT = "project";
    public static String PROJECTS = "projects";
    public static String FEEDBACK = "feedback";
    public static String FEEDBACKS = "feedbacks";
    public static String TEAM = "team";
    public static String TEAMS = "teams";

    private ModelAndViewConstant() {
    }
}
