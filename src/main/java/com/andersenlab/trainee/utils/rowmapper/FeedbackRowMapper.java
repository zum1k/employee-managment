package com.andersenlab.trainee.utils.rowmapper;

import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.utils.SQLConstant;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class FeedbackRowMapper implements RowMapper<Feedback> {
    @Override
    public Feedback mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Feedback feedback = new Feedback();
        feedback.setId(resultSet.getLong(SQLConstant.ID));
        feedback.setDescription(resultSet.getString(SQLConstant.FEEDBACK_DESCRIPTION));
        feedback.setCreateDate(resultSet.getDate(SQLConstant.FEEDBACK_CREATE_DATE).toLocalDate());
        return feedback;
    }
}
