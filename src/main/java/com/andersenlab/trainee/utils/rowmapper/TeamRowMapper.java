package com.andersenlab.trainee.utils.rowmapper;

import com.andersenlab.trainee.entity.Team;
import com.andersenlab.trainee.utils.SQLConstant;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TeamRowMapper implements RowMapper<Team> {
    @Override
    public Team mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Team team = new Team();
        team.setId(resultSet.getLong(SQLConstant.ID));
        team.setName(resultSet.getString(SQLConstant.TEAM_NAME));
        return team;
    }
}
