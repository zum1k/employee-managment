package com.andersenlab.trainee.utils.rowmapper;

import com.andersenlab.trainee.entity.DeveloperLevel;
import com.andersenlab.trainee.entity.Employee;
import com.andersenlab.trainee.entity.EnglishLevel;
import com.andersenlab.trainee.utils.SQLConstant;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class EmployeeRowMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getLong(SQLConstant.ID));
        employee.setName(resultSet.getString(SQLConstant.EMPLOYEE_NAME));
        employee.setSurname(resultSet.getString(SQLConstant.EMPLOYEE_SURNAME));
        employee.setPatronymic(resultSet.getString(SQLConstant.EMPLOYEE_PATRONYMIC));
        employee.setPhoneNumber(resultSet.getString(SQLConstant.EMPLOYEE_PHONE));
        employee.setBirthday(resultSet.getDate(SQLConstant.EMPLOYEE_BIRTHDAY).toLocalDate());
        employee.setDateOfEmployment(resultSet.getDate(SQLConstant.EMPLOYEE_DATE_OF_EMPLOYMENT).toLocalDate());
        employee.setProjectId(resultSet.getLong(SQLConstant.EMPLOYEE_PROJECT_ID));
        employee.setDeveloperLevel(DeveloperLevel.valueOf
                (resultSet.getString(SQLConstant.EMPLOYEE_DEVELOPER_LEVEL).toUpperCase()));
        employee.setEnglishLevel(EnglishLevel.valueOf
                (resultSet.getString(SQLConstant.EMPLOYEE_ENGLISH_LEVEL).toUpperCase()));
        employee.setSkype(resultSet.getString(SQLConstant.EMPLOYEE_SKYPE));
        employee.setFeedbackId(resultSet.getLong(SQLConstant.EMPLOYEE_FEEDBACK_ID));

        return employee;
    }
}
