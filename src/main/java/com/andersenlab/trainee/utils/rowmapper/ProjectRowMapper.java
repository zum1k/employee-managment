package com.andersenlab.trainee.utils.rowmapper;

import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.utils.SQLConstant;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
@Component
public class ProjectRowMapper implements RowMapper<Project> {
    @Override
    public Project mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Project project = new Project();
        project.setId(resultSet.getLong(SQLConstant.ID));
        project.setName(resultSet.getString(SQLConstant.PROJECT_NAME));
        project.setCustomer(resultSet.getString(SQLConstant.PROJECT_CUSTOMER));
        project.setDuration(resultSet.getLong(SQLConstant.PROJECT_DURATION));
        project.setMethodology(resultSet.getString(SQLConstant.PROJECT_METHODOLOGY));
        project.setProjectManagerId(resultSet.getLong(SQLConstant.PROJECT_PROJECT_MANAGER_ID));
        project.setTeamId(resultSet.getLong(SQLConstant.PROJECT_TEAM_ID));
        return project;
    }
}
