package com.andersenlab.trainee.utils.specitifcation.team;

import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.specitifcation.Specification;

public class TeamByNameSpecification implements Specification {
    private final String name;

    public TeamByNameSpecification(String name) {
        this.name = name;
    }

    @Override
    public String toSqlRequest() {
        return " AND " + SQLConstant.TEAM_NAME + " = ?";
    }

    @Override
    public Object[] receiveParameters() {
        return new Object[]{name};
    }
}
