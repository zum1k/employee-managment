package com.andersenlab.trainee.utils.specitifcation.employee;

import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.specitifcation.Specification;

public class EmployeeByNameSpecification implements Specification {
    private final String name;

    public EmployeeByNameSpecification(String name) {
        this.name = name;
    }

    @Override
    public String toSqlRequest() {
        return " AND " + SQLConstant.EMPLOYEE_NAME + " = ?";
    }

    @Override
    public Object[] receiveParameters() {
        return new Object[]{name};
    }
}
