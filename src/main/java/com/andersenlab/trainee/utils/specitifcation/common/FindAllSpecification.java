package com.andersenlab.trainee.utils.specitifcation.common;

import com.andersenlab.trainee.utils.specitifcation.Specification;

public class FindAllSpecification implements Specification {
    @Override
    public String toSqlRequest() {
        return " ; ";
    }

    @Override
    public Object[] receiveParameters() {
        return new Object[]{};
    }
}
