package com.andersenlab.trainee.utils.specitifcation.project;

import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.specitifcation.Specification;

public class ProjectByNameSpecification implements Specification {
    private final String name;

    public ProjectByNameSpecification(String name) {
        this.name = name;
    }

    @Override
    public String toSqlRequest() {
        return " AND " + SQLConstant.PROJECT_NAME + " = ?";
    }

    @Override
    public Object[] receiveParameters() {
        return new Object[]{name};
    }
}
