package com.andersenlab.trainee.utils.specitifcation;

public interface Specification {
    /**
     * @return sql query with (?) character instead of actual parameters
     */
    String toSqlRequest();

    /**
     * @return Array of all parameters that should participate in the query
     */
    Object[] receiveParameters();
}
