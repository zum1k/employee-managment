package com.andersenlab.trainee.utils.specitifcation.feedback;

import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.specitifcation.Specification;

public class FeedbackByDescriptionSpecification implements Specification {
    private final String description;

    public FeedbackByDescriptionSpecification(String name) {
        this.description = name;
    }

    @Override
    public String toSqlRequest() {
        return " AND " + SQLConstant.FEEDBACK_DESCRIPTION + " = ?";
    }

    @Override
    public Object[] receiveParameters() {
        return new Object[]{description};
    }
}
