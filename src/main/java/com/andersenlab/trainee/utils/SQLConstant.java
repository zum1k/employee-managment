package com.andersenlab.trainee.utils;

public class SQLConstant {
  public static final String ID = "id";
  //tables
  public static final String EMPLOYEE_TABLE_NAME = "employee";
  public static final String FEEDBACK_TABLE_NAME = "feedback";
  public static final String PROJECT_TABLE_NAME = "project";
  public static final String TEAM_TABLE_NAME = "team";
  //column names of EMPLOYEE
  public static final String EMPLOYEE_NAME = "name";
  public static final String EMPLOYEE_SURNAME = "surname";
  public static final String EMPLOYEE_PATRONYMIC = "patronymic";
  public static final String EMPLOYEE_PHONE = "phone_number";
  public static final String EMPLOYEE_BIRTHDAY = "birthday";
  public static final String EMPLOYEE_DATE_OF_EMPLOYMENT = "employment_date";
  public static final String EMPLOYEE_PROJECT_ID = "project_id";
  public static final String EMPLOYEE_DEVELOPER_LEVEL = "developer_level";
  public static final String EMPLOYEE_ENGLISH_LEVEL = "english_level";
  public static final String EMPLOYEE_SKYPE = "skype";
  public static final String EMPLOYEE_FEEDBACK_ID = "feedback_id";
  //column names of FEEDBACK
  public static final String FEEDBACK_DESCRIPTION = "description";
  public static final String FEEDBACK_CREATE_DATE = "create_date";
  //column names of PROJECT
  public static final String PROJECT_NAME = "name";
  public static final String PROJECT_CUSTOMER = "customer";
  public static final String PROJECT_DURATION = "duration";
  public static final String PROJECT_METHODOLOGY = "methodology";
  public static final String PROJECT_PROJECT_MANAGER_ID = "project_manager_id";
  public static final String PROJECT_TEAM_ID = "team_id";
  //column names of TEAM
  public static final String TEAM_NAME = "name";

  private SQLConstant() {
  }
}
