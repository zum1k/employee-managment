package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Feedback;

import java.util.List;
import java.util.Optional;

public interface FeedbackService {
    /**
     * @param feedback Feedback
     * @return primary key of last entry
     */
    Feedback add(Feedback feedback);

    /**
     * @param feedbackId Long
     * @return Optional<Feedback> if row exists
     */
    Optional<Feedback> findById(Long feedbackId);

    /**
     * @param feedback Feedback
     */
    Feedback update(Feedback feedback);

    /**
     * @param feedbackId Long
     */
    Feedback remove(Long feedbackId);

    /**
     * @return List<T> depending on the specification
     */
    List<Feedback> findAll();
}
