package com.andersenlab.trainee.service.impl;

import com.andersenlab.trainee.entity.Team;
import com.andersenlab.trainee.exception.EntityAlreadyExistsException;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.service.TeamService;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import com.andersenlab.trainee.utils.specitifcation.project.ProjectByNameSpecification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeamServiceImpl implements TeamService {
    private static final Logger logger = LogManager.getLogger(TeamServiceImpl.class);
    private final CommonRepository<Team, Long> repository;

    @Override
    public Team add(Team team) {
        logger.info("Add team {}", team);
        String teamName = team.getName();
        Optional<Team> optionalTeam = findByName(teamName);
        if (optionalTeam.isPresent()) {
            logger.info("Team {} already exists", teamName);
            throw new EntityAlreadyExistsException();
        }
        return repository.create(team);
    }

    @Override
    public Optional<Team> findById(Long teamId) {
        logger.info("Find team by id = {}", teamId);
        return repository.findById(teamId);
    }

    @Override
    public Team update(Team team) {
        logger.info("Update team by {}", team);
        return repository.update(team);
    }

    @Override
    public Team remove(Long teamId) {
        logger.info("Remove team by id = {}", teamId);
        return repository.remove(teamId);
    }

    @Override
    public List<Team> findAll() {
        logger.info("Find all teams");
        Specification specification = new FindAllSpecification();
        return repository.query(specification);
    }

    private Optional<Team> findByName(String teamName) {
        Specification specification = new ProjectByNameSpecification(teamName);
        return repository.singleQuery(specification);
    }
}
