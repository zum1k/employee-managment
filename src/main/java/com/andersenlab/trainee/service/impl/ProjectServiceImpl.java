package com.andersenlab.trainee.service.impl;

import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.exception.EntityAlreadyExistsException;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.service.ProjectService;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import com.andersenlab.trainee.utils.specitifcation.project.ProjectByNameSpecification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectServiceImpl implements ProjectService {
    private static final Logger logger = LogManager.getLogger(ProjectServiceImpl.class);
    private final CommonRepository<Project, Long> repository;

    @Override
    public Project add(Project project) {
        logger.info("Add project {}", project);
        String projectName = project.getName();
        Optional<Project> projectOptional = findByName(projectName);
        if (projectOptional.isPresent()) {
            logger.info("Project {} already exists", projectName);
            throw new EntityAlreadyExistsException();
        }
        return repository.create(project);
    }

    @Override
    public Optional<Project> findById(Long projectId) {
        logger.info("Find project by id = {}", projectId);
        return repository.findById(projectId);
    }

    @Override
    public Project update(Project project) {
        logger.info("Update project  {}", project);
        return repository.update(project);
    }

    @Override
    public Project remove(Long projectId) {
        logger.info("Remove project by id = {}", projectId);
        return repository.remove(projectId);
    }

    @Override
    public List<Project> findAll() {
        logger.info("Find all projects");
        Specification specification = new FindAllSpecification();
        return repository.query(specification);
    }

    private Optional<Project> findByName(String projectName) {
        Specification specification = new ProjectByNameSpecification(projectName);
        return repository.singleQuery(specification);
    }
}
