package com.andersenlab.trainee.service.impl;

import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.service.FeedbackService;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FeedbackServiceImpl implements FeedbackService {
    private static final Logger logger = LogManager.getLogger(FeedbackServiceImpl.class);
    private final CommonRepository<Feedback, Long> repository;

    @Override
    public Feedback add(Feedback feedback) {
        logger.info("Add feedback {}", feedback);
        return repository.create(feedback);
    }

    @Override
    public Optional<Feedback> findById(Long feedbackId) {
        logger.info("find feedback by id = {}", feedbackId);
        return repository.findById(feedbackId);
    }

    @Override
    public Feedback update(Feedback feedback) {
        logger.info("Update feedback {}", feedback);
        return repository.update(feedback);
    }

    @Override
    public Feedback remove(Long feedbackId) {
        logger.info("Remove feedback by id = {}", feedbackId);
        return repository.remove(feedbackId);
    }

    @Override
    public List<Feedback> findAll() {
        logger.info("Find all feedbacks");
        Specification specification = new FindAllSpecification();
        return repository.query(specification);
    }
}
