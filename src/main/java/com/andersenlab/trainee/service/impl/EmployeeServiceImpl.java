package com.andersenlab.trainee.service.impl;

import com.andersenlab.trainee.entity.Employee;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.service.EmployeeService;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeeServiceImpl implements EmployeeService {
    private static final Logger logger = LogManager.getLogger(EmployeeServiceImpl.class);
    private final CommonRepository<Employee, Long> repository;

    @Override
    public Employee add(Employee employee) {
        logger.info("Add employee {}", employee);
        return repository.create(employee);
    }

    @Override
    public Optional<Employee> findById(Long employeeId) {
        logger.info("find employee by id = {}", employeeId);
        return repository.findById(employeeId);
    }

    @Override
    public Employee update(Employee employee) {
        logger.info("Update employee by id={}, {}", employee.getId(), employee);
        return repository.update(employee);
    }

    @Override
    public Employee remove(Long employeeId) {
        logger.info("Remove employee by id = {}", employeeId);
        return repository.remove(employeeId);
    }

    @Override
    public List<Employee> findAll() {
        logger.info("Find all employees");
        Specification specification = new FindAllSpecification();
        return repository.query(specification);
    }
}
