package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {
    /**
     * @param team Team
     * @return primary key of last entry
     */
    Team add(Team team);

    /**
     * @param teamId Long
     * @return Optional<Team> if row exists
     */
    Optional<Team> findById(Long teamId);

    /**
     * @param team Team
     */
    Team update(Team team);

    /**
     * @param teamId Long
     */
    Team remove(Long teamId);

    /**
     * @return List<T> depending on the specification
     */
    List<Team> findAll();
}
