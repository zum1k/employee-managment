package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    /**
     * @param employee Employee
     * @return primary key of last entry
     */
    Employee add(Employee employee);

    /**
     * @param employeeId Long
     * @return Optional<Employee> if row exists
     */
    Optional<Employee> findById(Long employeeId);

    /**
     * @param employee Employee
     * @return Optional<Employee> if entity updated
     */
    Employee update(Employee employee);

    /**
     * @param employeeId Long
     */
    Employee remove(Long employeeId);

    /**
     * @return List<T> depending on the specification
     */
    List<Employee> findAll();
}
