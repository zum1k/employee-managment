package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectService {
    /**
     * @param project Project
     * @return Project if resource added
     */
    Project add(Project project);

    /**
     * @param projectId Long
     * @return Optional<Project> if row exists
     */
    Optional<Project> findById(Long projectId);

    /**
     * @param project Project
     * @return Project if resource updated
     */
    Project update(Project project);

    /**
     * @param projectId Long
     * @return Project if resource removed
     */
    Project remove(Long projectId);

    /**
     * @return List<T> depending on the specification
     */
    List<Project> findAll();
}
