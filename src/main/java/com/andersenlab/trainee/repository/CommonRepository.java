package com.andersenlab.trainee.repository;

import com.andersenlab.trainee.utils.specitifcation.Specification;

import java.util.List;
import java.util.Optional;

public interface CommonRepository<T, K> extends AutoCloseable {
    /**
     * @param t object to create
     * @return primary key of last entry
     */
    T create(T t);

    /**
     * @param t T (updated entity)
     */
    T update(T t);

    /**
     * @param id resource id
     */
    T remove(K id);

    /**
     * @param k resource id
     * @return Optional<T>
     */
    Optional<T> findById(K k);

    /**
     * @param specification Specification
     * @return List<T> depending on the specification
     */
    List<T> query(Specification specification);

    /**
     * @param specification Specification
     * @return <T> depending on the specification
     */
    Optional<T> singleQuery(Specification specification);
}
