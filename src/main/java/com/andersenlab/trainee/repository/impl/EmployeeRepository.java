package com.andersenlab.trainee.repository.impl;

import com.andersenlab.trainee.entity.Employee;
import com.andersenlab.trainee.exception.RepositoryException;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.rowmapper.EmployeeRowMapper;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeeRepository implements CommonRepository<Employee, Long> {
    private static final String INSERT_INTO_QUERY = "INSERT INTO employee" +
            "(name, surname, patronymic, phone_number,birthday,employment_date,project_id,developer_level, english_level,skype,feedback_id)" +
            " VALUES" +
            "(:name, :surname, :patronymic, :phone_number, :birthday, :employment_date, :project_id, :developer_level, :english_level, :skype, :feedback_id)";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM employee WHERE id = ?";
    private static final String UPDATE_BY_ID_QUERY = "UPDATE employee set" +
            " name = ?, surname = ?, patronymic = ?, phone_number = ?,birthday = ?,employment_date = ?,project_id = ?,developer_level = ?," +
            " english_level = ?,skype = ?,feedback_id = ? WHERE id = ?";
    private static final String SELECT_ALL_QUERY = "SELECT DISTINCT" +
            " employee.id, employee.name, employee.surname, employee.patronymic, employee.phone_number, employee.birthday, employee.employment_date," +
            " employee.project_id, employee.developer_level, employee.english_level, employee.skype, employee.feedback_id " +
            "from employee WHERE 1=1 ";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM employee WHERE employee.id = ?";

    private static final Logger logger = LogManager.getLogger(EmployeeRepository.class);
    private final EmployeeRowMapper mapper;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Employee create(Employee employee) {
        logger.info("add employee");
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue(SQLConstant.EMPLOYEE_NAME, employee.getName())
                .addValue(SQLConstant.EMPLOYEE_SURNAME, employee.getSurname())
                .addValue(SQLConstant.EMPLOYEE_PATRONYMIC, employee.getPatronymic())
                .addValue(SQLConstant.EMPLOYEE_PHONE, employee.getPhoneNumber())
                .addValue(SQLConstant.EMPLOYEE_BIRTHDAY, employee.getBirthday())
                .addValue(SQLConstant.EMPLOYEE_DATE_OF_EMPLOYMENT, employee.getDateOfEmployment())
                .addValue(SQLConstant.EMPLOYEE_PROJECT_ID, employee.getProjectId())
                .addValue(SQLConstant.EMPLOYEE_DEVELOPER_LEVEL, employee.getDeveloperLevel().toString())
                .addValue(SQLConstant.EMPLOYEE_ENGLISH_LEVEL, employee.getEnglishLevel().toString())
                .addValue(SQLConstant.EMPLOYEE_SKYPE, employee.getSkype())
                .addValue(SQLConstant.EMPLOYEE_FEEDBACK_ID, employee.getFeedbackId());

        namedParameterJdbcTemplate.update(INSERT_INTO_QUERY, parameters, holder);
        Integer id = (Integer) Objects.requireNonNull(Objects.requireNonNull(holder.getKeys()).get(SQLConstant.ID));
        employee.setId(id.longValue());
        return employee;
    }

    @Override
    public Employee update(Employee employee) {
        logger.info("update employee");
        Long employeeId = employee.getId();
        if (jdbcTemplate.update(
                UPDATE_BY_ID_QUERY,
                employee.getName(),
                employee.getSurname(),
                employee.getPatronymic(),
                employee.getPhoneNumber(),
                employee.getBirthday(),
                employee.getDateOfEmployment(),
                employee.getProjectId(),
                employee.getDeveloperLevel().toString(),
                employee.getEnglishLevel().toString(),
                employee.getSkype(),
                employee.getFeedbackId(),
                employeeId) == 0) {
            throw new RepositoryException("Employee not updated ", employeeId);
        }
        return employee;
    }

    @Override
    public Employee remove(Long id) {
        logger.info("remove employee by id = {}", id);
        Optional<Employee> employee = findById(id);
        if (employee.isEmpty()) {
            logger.info("Employee {} not deleted", id);
            throw new RepositoryException("Employee not deleted");
        }
        jdbcTemplate.update(DELETE_BY_ID_QUERY, id);
        return employee.get();
    }

    @Override
    public Optional<Employee> findById(Long id) {
        logger.info("find employee by id ={}", id);
        List<Employee> resultSet = jdbcTemplate.query(SELECT_BY_ID_QUERY, mapper, id);
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public List<Employee> query(Specification specification) {
        logger.info("find all with parameters");
        return jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
    }

    @Override
    public Optional<Employee> singleQuery(Specification specification) {
        logger.info("find employee by specification");
        List<Employee> resultSet = jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public void close() throws Exception {
    }
}
