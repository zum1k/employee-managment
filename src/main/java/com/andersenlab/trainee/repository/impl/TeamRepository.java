package com.andersenlab.trainee.repository.impl;

import com.andersenlab.trainee.entity.Team;
import com.andersenlab.trainee.exception.RepositoryException;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.rowmapper.TeamRowMapper;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeamRepository implements CommonRepository<Team, Long> {
    private static final String INSERT_INTO_QUERY = "INSERT INTO team (name) VALUES (:name)";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM team WHERE id = ?";
    private static final String UPDATE_BY_ID_QUERY = "UPDATE team set name = ? WHERE id = ?";
    private static final String SELECT_ALL_QUERY = "SELECT DISTINCT team.id, team.name from team WHERE 1=1 ";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM team WHERE team.id = ?";

    private static final Logger logger = LogManager.getLogger(TeamRepository.class);
    private final TeamRowMapper mapper;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Team create(Team team) {
        logger.info("add team");
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue(SQLConstant.TEAM_NAME, team.getName());
        namedParameterJdbcTemplate.update(INSERT_INTO_QUERY, parameters, holder);
        Integer id = (Integer) Objects.requireNonNull(Objects.requireNonNull(holder.getKeys()).get(SQLConstant.ID));
        team.setId(id.longValue());
        return team;
    }

    @Override
    public Team update(Team team) {
        logger.info("update team");
        Long teamId = team.getId();
        if (jdbcTemplate.update(UPDATE_BY_ID_QUERY, team.getName(), teamId) == 0) {
            throw new RepositoryException("Team not updated ", teamId);
        }
        return team;
    }

    @Override
    public Team remove(Long id) {
        logger.info("remove team by id = {}", id);
        Optional<Team> team = findById(id);
        if (team.isEmpty()) {
            logger.info("Team not deleted");
            throw new RepositoryException("Team not deleted");
        }
        jdbcTemplate.update(DELETE_BY_ID_QUERY, id);
        return team.get();
    }

    @Override
    public Optional<Team> findById(Long id) {
        logger.info("find team by id ={}", id);
        List<Team> resultSet = jdbcTemplate.query(SELECT_BY_ID_QUERY, mapper, id);
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public List<Team> query(Specification specification) {
        logger.info("find all with parameters");
        return jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
    }

    @Override
    public Optional<Team> singleQuery(Specification specification) {
        logger.info("find team by specification");
        List<Team> resultSet = jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public void close() throws Exception {
    }
}
