package com.andersenlab.trainee.repository.impl;

import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.exception.RepositoryException;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.rowmapper.ProjectRowMapper;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectRepository implements CommonRepository<Project, Long> {
    private static final String INSERT_INTO_QUERY = "INSERT INTO project (name,customer, duration,methodology,project_manager_id,team_id)" +
            " VALUES (:name, :customer, :duration, :methodology, :project_manager_id,:team_id)";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM project WHERE id = ?";
    private static final String UPDATE_BY_ID_QUERY = "UPDATE project set" +
            " name = ?,customer = ?, duration = ?,methodology = ?,project_manager_id = ?,team_id = ? WHERE id = ?";
    private static final String SELECT_ALL_QUERY = "SELECT DISTINCT" +
            " project.id, project.name,project.customer, project.duration,project.methodology,project.project_manager_id,project.team_id from project WHERE 1=1 ";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM project WHERE project.id = ?";

    private static final Logger logger = LogManager.getLogger(ProjectRepository.class);
    private final ProjectRowMapper mapper;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Project create(Project project) {
        logger.info("add project");
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue(SQLConstant.PROJECT_NAME, project.getName())
                .addValue(SQLConstant.PROJECT_CUSTOMER, project.getCustomer())
                .addValue(SQLConstant.PROJECT_DURATION, project.getDuration())
                .addValue(SQLConstant.PROJECT_METHODOLOGY, project.getMethodology())
                .addValue(SQLConstant.PROJECT_PROJECT_MANAGER_ID, project.getProjectManagerId())
                .addValue(SQLConstant.PROJECT_TEAM_ID, project.getTeamId());
        namedParameterJdbcTemplate.update(INSERT_INTO_QUERY, parameters, holder);
        Integer id = (Integer) Objects.requireNonNull(Objects.requireNonNull(holder.getKeys()).get(SQLConstant.ID));
        project.setId(id.longValue());
        return project;
    }

    @Override
    public Project update(Project project) {
        logger.info("update project");
        Long projectId = project.getId();
        if (jdbcTemplate.update(
                UPDATE_BY_ID_QUERY,
                project.getName(),
                project.getCustomer(),
                project.getDuration(),
                project.getMethodology(),
                project.getProjectManagerId(),
                project.getTeamId(),
                projectId) == 0) {
            throw new RepositoryException("Project not updated ", projectId);
        }
        return project;
    }

    @Override
    public Project remove(Long id) {
        logger.info("remove project by id = {}", id);
        Optional<Project> project = findById(id);
        if (project.isEmpty()) {
            logger.info("Project not deleted");
            throw new RepositoryException("Project not deleted");
        }
        jdbcTemplate.update(DELETE_BY_ID_QUERY, id);
        return project.get();
    }

    @Override
    public Optional<Project> findById(Long id) {
        logger.info("find project by id ={}", id);
        List<Project> resultSet = jdbcTemplate.query(SELECT_BY_ID_QUERY, mapper, id);
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public List<Project> query(Specification specification) {
        logger.info("find all with parameters");
        return jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
    }

    @Override
    public Optional<Project> singleQuery(Specification specification) {
        logger.info("find project by specification");
        List<Project> resultSet = jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public void close() throws Exception {
    }

}
