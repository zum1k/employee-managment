package com.andersenlab.trainee.repository.impl;

import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.exception.RepositoryException;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.utils.SQLConstant;
import com.andersenlab.trainee.utils.rowmapper.FeedbackRowMapper;
import com.andersenlab.trainee.utils.specitifcation.Specification;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FeedbackRepository implements CommonRepository<Feedback, Long> {
    private static final String INSERT_INTO_QUERY = "INSERT INTO feedback (description, create_date)" +
            " VALUES (:description, :create_date)";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM feedback WHERE id = ?";
    private static final String UPDATE_BY_ID_QUERY = "UPDATE feedback set" +
            " description = ?, create_date = ? WHERE id = ?";
    private static final String SELECT_ALL_QUERY = "SELECT DISTINCT" +
            " feedback.id, feedback.description, feedback.create_date from feedback WHERE 1=1 ";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM feedback WHERE feedback.id = ?";

    private static final Logger logger = LogManager.getLogger(FeedbackRepository.class);
    private final FeedbackRowMapper mapper;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Feedback create(Feedback feedback) {
        logger.info("add feedback");
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue(SQLConstant.FEEDBACK_DESCRIPTION, feedback.getDescription())
                .addValue(SQLConstant.FEEDBACK_CREATE_DATE, feedback.getCreateDate());
        namedParameterJdbcTemplate.update(INSERT_INTO_QUERY, parameters, holder);
        Integer id = (Integer) Objects.requireNonNull(Objects.requireNonNull(holder.getKeys()).get(SQLConstant.ID));
        feedback.setId(id.longValue());
        return feedback;
    }

    @Override
    public Feedback update(Feedback feedback) {
        logger.info("update employee");
        Long feedbackId = feedback.getId();
        if (jdbcTemplate.update(
                UPDATE_BY_ID_QUERY,
                feedback.getDescription(),
                feedback.getCreateDate(),
                feedbackId) == 0) {
            throw new RepositoryException("Feedback not updated ", feedbackId);
        }
        return feedback;
    }

    @Override
    public Feedback remove(Long id) {
        logger.info("remove feedback by id = {}", id);
        Optional<Feedback> feedback = findById(id);
        if (feedback.isEmpty()) {
            logger.info("Feedback not deleted");
            throw new RepositoryException("Feedback not deleted");
        }
        jdbcTemplate.update(DELETE_BY_ID_QUERY, id);
        return feedback.get();
    }

    @Override
    public Optional<Feedback> findById(Long id) {
        logger.info("find feedback by id ={}", id);
        List<Feedback> resultSet = jdbcTemplate.query(SELECT_BY_ID_QUERY, mapper, id);
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public List<Feedback> query(Specification specification) {
        logger.info("find all with parameters");
        return jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
    }

    @Override
    public Optional<Feedback> singleQuery(Specification specification) {
        logger.info("find feedback by specification");
        List<Feedback> resultSet = jdbcTemplate.query(SELECT_ALL_QUERY + specification.toSqlRequest(), mapper, specification.receiveParameters());
        return resultSet.size() == 1 ? Optional.of(resultSet.get(0)) : Optional.empty();
    }

    @Override
    public void close() throws Exception {
    }
}
