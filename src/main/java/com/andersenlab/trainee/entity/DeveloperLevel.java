package com.andersenlab.trainee.entity;

public enum DeveloperLevel {
    J1,
    J2,
    M1,
    M2,
    M3,
    S1,
    S2
}
