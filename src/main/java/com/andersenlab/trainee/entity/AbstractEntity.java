package com.andersenlab.trainee.entity;

import lombok.Data;

@Data
public abstract class AbstractEntity<T extends Long> {
  private T id;
}
