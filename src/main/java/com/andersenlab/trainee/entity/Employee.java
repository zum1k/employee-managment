package com.andersenlab.trainee.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class Employee extends AbstractEntity<Long> {
    private String name;
    private String surname;
    private String patronymic;
    private String phoneNumber;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate birthday;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate dateOfEmployment;
    private Long projectId;
    private DeveloperLevel developerLevel;
    private EnglishLevel englishLevel;
    private String skype;
    private Long feedbackId;
}
