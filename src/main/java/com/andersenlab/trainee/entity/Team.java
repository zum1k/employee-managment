package com.andersenlab.trainee.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Team extends AbstractEntity<Long> {
  private String name;
}
