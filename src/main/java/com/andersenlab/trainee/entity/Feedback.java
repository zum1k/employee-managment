package com.andersenlab.trainee.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class Feedback extends AbstractEntity<Long> {
  private String description;
  private LocalDate createDate;
}
