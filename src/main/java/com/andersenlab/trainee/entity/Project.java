package com.andersenlab.trainee.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data

public class Project extends AbstractEntity<Long> {
  private String name;
  private String customer;
  private Long duration;
  private String methodology;
  private Long projectManagerId;
  private Long teamId;
}
