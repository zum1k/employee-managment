package com.andersenlab.trainee.exception;

public class CommandException extends RuntimeException{
    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable e) {
        super(message, e);
    }

    public CommandException(Throwable e) {
        super(e);
    }
}
