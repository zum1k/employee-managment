package com.andersenlab.trainee.exception;

public class RepositoryException extends RuntimeException {
  private Long id;
  public RepositoryException() {
  }
  public RepositoryException(String message, Long id){
    super(message);
    this.id = id;
  }

  public RepositoryException(String message) {
    super(message);
  }

  public RepositoryException(String message, Throwable cause) {
    super(message, cause);
  }

  public RepositoryException(Throwable cause) {
    super(cause);
  }
}
