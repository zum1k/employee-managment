package com.andersenlab.trainee.exception;

public class ConnectionException extends RuntimeException {
        public ConnectionException(String message) {
            super(message);
        }

        public ConnectionException(String message, Throwable e) {
            super(message, e);
        }

        public ConnectionException(Throwable e) {
            super(e);
        }


}
