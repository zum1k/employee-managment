package com.andersenlab.trainee.configuration;

import com.andersenlab.trainee.repository.impl.EmployeeRepository;
import com.andersenlab.trainee.repository.impl.FeedbackRepository;
import com.andersenlab.trainee.repository.impl.ProjectRepository;
import com.andersenlab.trainee.repository.impl.TeamRepository;
import com.andersenlab.trainee.utils.rowmapper.EmployeeRowMapper;
import com.andersenlab.trainee.utils.rowmapper.FeedbackRowMapper;
import com.andersenlab.trainee.utils.rowmapper.ProjectRowMapper;
import com.andersenlab.trainee.utils.rowmapper.TeamRowMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DBTestConfig {
    private static final String SCRIPT_ENCODING = "UTF-8";

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost/employees_test_db");
        dataSource.setUsername("postgres");
        dataSource.setPassword("2584170");
        return dataSource;
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public EmployeeRepository employeeRepository() {
        return new EmployeeRepository(new EmployeeRowMapper(), jdbcTemplate(), namedParameterJdbcTemplate());
    }

    @Bean
    public ProjectRepository projectRepository() {
        return new ProjectRepository(new ProjectRowMapper(), jdbcTemplate(), namedParameterJdbcTemplate());
    }

    @Bean
    public FeedbackRepository feedbackRepository() {
        return new FeedbackRepository(new FeedbackRowMapper(), jdbcTemplate(), namedParameterJdbcTemplate());
    }

    @Bean
    public TeamRepository teamRepository() {
        return new TeamRepository(new TeamRowMapper(), jdbcTemplate(), namedParameterJdbcTemplate());
    }


}
