package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.entity.Team;
import com.andersenlab.trainee.repository.impl.TeamRepository;
import com.andersenlab.trainee.service.impl.TeamServiceImpl;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTest {
    @InjectMocks
    private TeamServiceImpl service;
    @Mock
    private TeamRepository repository;
    private Long expectedId;
    private Team expectedTeam;

    @Before
    public void init() {
        expectedId = 1L;
        String expectedName = "expectedName1";
        expectedTeam = new Team();
        expectedTeam.setName(expectedName);
        expectedTeam.setId(expectedId);
    }

    @Test
    public void add_ShouldReturn_Number() {
        Mockito.when(repository.create(Mockito.any(Team.class))).thenReturn(expectedTeam);
        Team actualTeam
                = service.add(expectedTeam);
        Assert.assertEquals(expectedTeam, actualTeam);
    }

    @Test
    public void findById_ShouldReturn_Entity() {
        expectedTeam.setId(expectedId);
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.of(expectedTeam));

        Long actualId = service.findById(expectedId).get().getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findById_ShouldReturn_Empty() {
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.empty());

        Optional<Team> actualOptional = service.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }

    @Test
    public void update_Should_Update() {
        Mockito.when(repository.update(Mockito.any(Team.class))).thenReturn(expectedTeam);
        Team actualTeam = service.update(expectedTeam);
        Assert.assertEquals(expectedTeam, actualTeam);
    }

    @Test
    public void remove_Should_Remove() {
            Mockito.when(repository.remove(expectedId)).thenReturn(expectedTeam);
            Long actualId = service.remove(expectedId).getId();
            Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findAll_ShouldReturn_List() {
        List<Team> teams = new ArrayList<>();
        teams.add(expectedTeam);
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(teams);
        List<Team> actualList = service.findAll();
        Assert.assertEquals(teams, actualList);
    }

    @Test
    public void findAll_ShouldReturn_EmptyList() {
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(Collections.emptyList());
        List<Team> actualList = service.findAll();
        Assert.assertTrue(actualList.isEmpty());
    }
}