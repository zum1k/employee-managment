package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.repository.CommonRepository;
import com.andersenlab.trainee.repository.impl.ProjectRepository;
import com.andersenlab.trainee.service.impl.ProjectServiceImpl;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceImplTest {
    @InjectMocks
    private ProjectServiceImpl service;
    @Mock
    private ProjectRepository repository;
    private Long expectedId;
    private Project expectedProject;

    @Before
    public void init() {
        expectedId = 1L;
        String expectedName = "expectedName1";
        expectedProject = new Project();
        expectedProject.setName(expectedName);
        expectedProject.setId(expectedId);
    }

    @Test
    public void add_ShouldReturn_Number() {
        Mockito.when(repository.create(Mockito.any(Project.class))).thenReturn(expectedProject);
        Project actualProject = service.add(expectedProject);
        Assert.assertEquals(expectedProject, actualProject);
    }

    @Test
    public void findById_ShouldReturn_Entity() {
        expectedProject.setId(expectedId);
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.of(expectedProject));

        Long actualId = service.findById(expectedId).get().getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findById_ShouldReturn_Empty() {
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.empty());

        Optional<Project> actualOptional = service.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }

    @Test
    public void update_Should_Update() {
        Mockito.when(repository.update(Mockito.any(Project.class))).thenReturn(expectedProject);
        Project actualProject = service.update(expectedProject);
        Assert.assertEquals(expectedProject, actualProject);
    }

    @Test
    public void remove_Should_Remove() {
        Mockito.when(repository.remove(expectedId)).thenReturn(expectedProject);
        Long actualId = service.remove(expectedId).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findAll_ShouldReturn_List() {
        List<Project> projects = new ArrayList<>();
        projects.add(expectedProject);
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(projects);
        List<Project> actualList = service.findAll();
        Assert.assertEquals(projects, actualList);
    }

    @Test
    public void findAll_ShouldReturn_EmptyList() {
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(Collections.emptyList());
        List<Project> actualList = service.findAll();
        Assert.assertTrue(actualList.isEmpty());
    }
}