package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.DeveloperLevel;
import com.andersenlab.trainee.entity.Employee;
import com.andersenlab.trainee.entity.EnglishLevel;
import com.andersenlab.trainee.repository.impl.EmployeeRepository;
import com.andersenlab.trainee.service.impl.EmployeeServiceImpl;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {
    @InjectMocks
    private EmployeeServiceImpl service;
    @Mock
    private EmployeeRepository repository;
    private Long expectedId;
    private Employee expectedEmployee;

    @Before
    public void init() {
        expectedId = 1L;
        String expectedName = "expectedName";
        expectedEmployee = new Employee();
        expectedEmployee.setId(expectedId);
        expectedEmployee.setName("expected_name_5");
        expectedEmployee.setSurname("expected_surname_5");
        expectedEmployee.setPatronymic("expected_patronymic_5");
        expectedEmployee.setPhoneNumber("37525 1111111");
        expectedEmployee.setBirthday(LocalDate.now());
        expectedEmployee.setDateOfEmployment(LocalDate.now());
        expectedEmployee.setProjectId(1L);
        expectedEmployee.setDeveloperLevel(DeveloperLevel.J2);
        expectedEmployee.setEnglishLevel(EnglishLevel.A2);
        expectedEmployee.setSkype("expected_skype_5");
        expectedEmployee.setFeedbackId(2L);
    }

    @Test
    public void add_ShouldReturn_Number() {
        Mockito.when(repository.create(expectedEmployee)).thenReturn(expectedEmployee);
        Employee actualEmployee = service.add(expectedEmployee);
        Assert.assertEquals(expectedEmployee, actualEmployee);
    }

    @Test
    public void findById_ShouldReturn_Entity() {
        expectedEmployee.setId(expectedId);
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.of(expectedEmployee));

        Long actualId = service.findById(expectedId).get().getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findById_ShouldReturn_Empty() {
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.empty());

        Optional<Employee> actualOptional = service.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }

    @Test
    public void update_ShouldReturn_Employee() {
        Mockito.when(repository.update(Mockito.any(Employee.class))).thenReturn(expectedEmployee);
        Employee actualEmployee = service.update(expectedEmployee);
        Assert.assertEquals(expectedEmployee, actualEmployee);
    }

    @Test
    public void remove_ShouldReturn_Employee() {
        Mockito.when(repository.remove(expectedId)).thenReturn(expectedEmployee);
        Long actualId = service.remove(expectedId).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findAll_ShouldReturn_List() {
        List<Employee> employees = new ArrayList<>();
        employees.add(expectedEmployee);
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(employees);
        List<Employee> actualList = service.findAll();
        Assert.assertEquals(employees, actualList);
    }

    @Test
    public void findAll_ShouldReturn_EmptyList() {
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(Collections.emptyList());
        List<Employee> actualList = service.findAll();
        Assert.assertTrue(actualList.isEmpty());
    }
}