package com.andersenlab.trainee.service;

import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.repository.impl.FeedbackRepository;
import com.andersenlab.trainee.service.impl.FeedbackServiceImpl;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class FeedbackServiceImplTest {
    @InjectMocks
    private FeedbackServiceImpl service;
    @Mock
    private FeedbackRepository repository;
    private Long expectedId;
    private Feedback expectedFeedback;

    @Before
    public void init() {
        expectedId = 1L;
        String expectedDescription = "expectedDescription1";
        expectedFeedback = new Feedback();
        expectedFeedback.setDescription(expectedDescription);
        expectedFeedback.setId(expectedId);
    }

    @Test
    public void add_ShouldReturn_Number() {
        Mockito.when(repository.create(Mockito.any(Feedback.class))).thenReturn(expectedFeedback);
        Feedback actualFeedback = service.add(expectedFeedback);
        Assert.assertEquals(expectedFeedback, actualFeedback);
    }

    @Test
    public void findById_ShouldReturn_Entity() {
        expectedFeedback.setId(expectedId);
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.of(expectedFeedback));

        Long actualId = service.findById(expectedId).get().getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findById_ShouldReturn_Empty() {
        Mockito.when(repository.findById(expectedId))
                .thenReturn(Optional.empty());

        Optional<Feedback> actualOptional = service.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }

    @Test
    public void update_Should_Update() {
        Mockito.when(repository.update(Mockito.any(Feedback.class))).thenReturn(expectedFeedback);
        Feedback actualFeedback = service.update(expectedFeedback);
        Assert.assertEquals(expectedFeedback, actualFeedback);
    }

    @Test
    public void remove_Should_Remove() {
        Mockito.when(repository.remove(expectedId)).thenReturn(expectedFeedback);
        Long actualId = service.remove(expectedId).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void findAll_ShouldReturn_List() {
        List<Feedback> feedbacks = new ArrayList<>();
        feedbacks.add(expectedFeedback);
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(feedbacks);
        List<Feedback> actualList = service.findAll();
        Assert.assertEquals(feedbacks, actualList);
    }

    @Test
    public void findAll_ShouldReturn_EmptyList() {
        Mockito.when(repository.query(Mockito.any(FindAllSpecification.class))).thenReturn(Collections.emptyList());
        List<Feedback> actualList = service.findAll();
        Assert.assertTrue(actualList.isEmpty());
    }
}