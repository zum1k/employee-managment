package com.andersenlab.trainee.repository;

import com.andersenlab.trainee.configuration.DBTestConfig;
import com.andersenlab.trainee.entity.Feedback;
import com.andersenlab.trainee.repository.impl.FeedbackRepository;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import com.andersenlab.trainee.utils.specitifcation.feedback.FeedbackByDescriptionSpecification;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ContextConfiguration(classes = {DBTestConfig.class})
@RunWith(SpringRunner.class)
public class FeedBackRepositoryTest {
    private static final String SCRIPT_PATH = "src/test/resources/database/feedback-script.sql";
    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS feedback;";
    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE feedback" +
                    " (" +
                    " id               INTEGER GENERATED ALWAYS AS IDENTITY  PRIMARY KEY," +
                    " description      CHARACTER VARYING(400)       NOT NULL," +
                    " create_date      DATE                         NOT NULL);";
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private FeedbackRepository repository;
    private String expectedDescription;
    private Feedback expectedFeedback;
    private Long expectedId;

    @Before
    public void init() {
        expectedDescription = "description_1";
        expectedId = 5L;

        expectedFeedback = new Feedback();
        expectedFeedback.setDescription(expectedDescription);
        expectedFeedback.setCreateDate(LocalDate.now());

        jdbcTemplate.execute(SQL_DROP_TABLE);
        jdbcTemplate.execute(SQL_CREATE_TABLE);
        try {
            List<String> lines = Files.readAllLines(Paths.get(SCRIPT_PATH), Charset.defaultCharset());
            lines.forEach(jdbcTemplate::execute);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deInit() {
        jdbcTemplate.execute("DELETE FROM feedback");
    }

    @Test
    public void add_ShouldReturn_Feedback_Test() {
        long expectedId = 6;
        long actualId = repository.create(expectedFeedback).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void singleQuery_ShouldReturn_Entity_Test() {
        String actualDescription = repository.singleQuery(new FeedbackByDescriptionSpecification(expectedDescription)).get().getDescription();
        Assert.assertEquals(expectedDescription, actualDescription);
    }

    @Test
    public void singleQuery_ShouldReturn_Empty_Test() {
        String expectedDescription = "desc";
        Optional<Feedback> optionalEmployee = repository.singleQuery(
                new FeedbackByDescriptionSpecification(expectedDescription));
        Assert.assertTrue(optionalEmployee.isEmpty());
    }

    @Test
    public void query_ShouldReturn_List_Test() {
        int expectedSize = 5;
        int actualSize = repository.query(new FindAllSpecification()).size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void remove_ShouldReturn_ExpectedNumber_Test() {
        Long actualId = repository.remove(expectedId).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void update_ShouldReturn_ExpectedNumber_Test() {
        String expectedName = "update";
        expectedFeedback.setDescription(expectedName);
        expectedFeedback.setId(expectedId);
        repository.update(expectedFeedback);
        String actualName = repository.update(expectedFeedback).getDescription();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void findById_ShouldReturn_OptionalOfEntity_Test() {
        Optional<Feedback> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isPresent());
    }

    @Test
    public void findById_ShouldReturn_Empty_Test() {
        long expectedId = 1000;
        Optional<Feedback> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }
}
