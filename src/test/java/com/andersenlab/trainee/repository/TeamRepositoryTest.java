package com.andersenlab.trainee.repository;

import com.andersenlab.trainee.configuration.DBTestConfig;
import com.andersenlab.trainee.entity.Team;
import com.andersenlab.trainee.repository.impl.TeamRepository;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import com.andersenlab.trainee.utils.specitifcation.team.TeamByNameSpecification;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@ContextConfiguration(classes = {DBTestConfig.class})
@RunWith(SpringRunner.class)
public class TeamRepositoryTest {
    private static final String SCRIPT_PATH = "src/test/resources/database/team-script.sql";
    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS team;";
    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE team" +
                    " (" +
                    " id               INTEGER GENERATED ALWAYS AS IDENTITY  PRIMARY KEY," +
                    " name             CHARACTER VARYING(100)        NOT NULL);";
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TeamRepository repository;
    private Team expectedTeam;
    private String expectedName;
    private Long expectedId;

    @Before
    public void init() {
        expectedName = "expected_name_1";
        expectedId = 5L;

        expectedTeam = new Team();
        expectedTeam.setName("expected_name");

        jdbcTemplate.execute(SQL_DROP_TABLE);
        jdbcTemplate.execute(SQL_CREATE_TABLE);
        try {
            List<String> lines = Files.readAllLines(Paths.get(SCRIPT_PATH), Charset.defaultCharset());
            lines.forEach(jdbcTemplate::execute);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deInit() {
        jdbcTemplate.execute("DELETE FROM feedback");
    }

    @Test
    public void add_ShouldReturn_Number_Test() {
        long expectedId = 6;
        long actualId = repository.create(expectedTeam).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void singleQuery_ShouldReturn_Entity_Test() {
        String actualName = repository.singleQuery(new TeamByNameSpecification(expectedName)).get().getName();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void singleQuery_ShouldReturn_Empty_Test() {
        String expectedName = "expected1000";
        Optional<Team> optionalEmployee = repository.singleQuery(new TeamByNameSpecification(expectedName));
        Assert.assertTrue(optionalEmployee.isEmpty());
    }

    @Test
    public void query_ShouldReturn_List_Test() {
        int expectedSize = 5;
        int actualSize = repository.query(new FindAllSpecification()).size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void remove_ShouldReturn_ExpectedNumber_Test() {
        Long actualId = repository.remove(expectedId).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void update_ShouldReturn_ExpectedNumber_Test() {
        String expectedName = "update";
        expectedTeam.setName(expectedName);
        expectedTeam.setId(expectedId);
        repository.update(expectedTeam);
        String actualName = repository.update(expectedTeam).getName();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void findById_ShouldReturn_OptionalOfEntity_Test() {
        Optional<Team> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isPresent());
    }

    @Test
    public void findById_ShouldReturn_Empty_Test() {
        long expectedId = 1000;
        Optional<Team> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }
}
