package com.andersenlab.trainee.repository;

import com.andersenlab.trainee.configuration.DBTestConfig;
import com.andersenlab.trainee.entity.Project;
import com.andersenlab.trainee.repository.impl.ProjectRepository;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import com.andersenlab.trainee.utils.specitifcation.project.ProjectByNameSpecification;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@ContextConfiguration(classes = {DBTestConfig.class})
@RunWith(SpringRunner.class)
public class ProjectRepositoryTest {
    private static final String SCRIPT_PATH = "src/test/resources/database/project-script.sql";
    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS project;";
    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE project" +
                    " (" +
                    " id               INTEGER GENERATED ALWAYS AS IDENTITY  PRIMARY KEY," +
                    " name             CHARACTER VARYING(100)        NOT NULL," +
                    " customer         CHARACTER VARYING(100)        NOT NULL," +
                    " duration         INTEGER                       NOT NULL," +
                    "methodology       CHARACTER VARYING(150)        NOT NULL," +
                    "project_manager_id INTEGER                      NOT NULL," +
                    "team_id            INTEGER                      NOT NULL);";
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ProjectRepository repository;
    private String expectedName;
    private Project expectedProject;
    private Long expectedId;

    @Before
    public void init() {
        expectedId = 5L;
        expectedName = "expected_name_1";
        expectedProject = new Project();
        expectedProject.setName(expectedName);
        expectedProject.setCustomer("expected_customer");
        expectedProject.setDuration(300L);
        expectedProject.setMethodology("expected_methodology");
        expectedProject.setProjectManagerId(10L);
        expectedProject.setTeamId(10L);

        jdbcTemplate.execute(SQL_DROP_TABLE);
        jdbcTemplate.execute(SQL_CREATE_TABLE);
        try {
            List<String> lines = Files.readAllLines(Paths.get(SCRIPT_PATH), Charset.defaultCharset());
            lines.forEach(jdbcTemplate::execute);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deInit() {
        jdbcTemplate.execute("DELETE FROM project");
    }

    @Test
    public void add_ShouldReturn_Number_Test() {
        long expectedId = 6;
        long actualId = repository.create(expectedProject).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void singleQuery_ShouldReturn_Entity_Test() {
        String actualName = repository.singleQuery(new ProjectByNameSpecification(expectedName)).get().getName();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void singleQuery_ShouldReturn_Empty_Test() {
        String expectedName = "bname";
        Optional<Project> optionalEmployee = repository.singleQuery(new ProjectByNameSpecification(expectedName));
        Assert.assertTrue(optionalEmployee.isEmpty());
    }

    @Test
    public void query_ShouldReturn_List_Test() {
        int expectedSize = 5;
        int actualSize = repository.query(new FindAllSpecification()).size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void remove_ShouldReturn_ExpectedNumber_Test() {
        Long actualId = repository.remove(expectedId).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void update_ShouldReturn_ExpectedNumber_Test() {
        String expectedName = "update";
        expectedProject.setName(expectedName);
        expectedProject.setId(expectedId);
        repository.update(expectedProject);
        String actualName = repository.update(expectedProject).getName();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void findById_ShouldReturn_OptionalOfEntity_Test() {
        Optional<Project> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isPresent());
    }

    @Test
    public void findById_ShouldReturn_Empty_Test() {
        long expectedId = 1000;
        Optional<Project> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }
}
