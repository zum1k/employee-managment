package com.andersenlab.trainee.repository;

import com.andersenlab.trainee.configuration.DBTestConfig;
import com.andersenlab.trainee.entity.DeveloperLevel;
import com.andersenlab.trainee.entity.Employee;
import com.andersenlab.trainee.entity.EnglishLevel;
import com.andersenlab.trainee.repository.impl.EmployeeRepository;
import com.andersenlab.trainee.utils.specitifcation.common.FindAllSpecification;
import com.andersenlab.trainee.utils.specitifcation.employee.EmployeeByNameSpecification;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ContextConfiguration(classes = {DBTestConfig.class})
@RunWith(SpringRunner.class)
public class EmployeeRepositoryTest {
    private static final String SCRIPT_PATH = "src/test/resources/database/employee-script.sql";
    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS employee;";
    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE employee" +
                    " (" +
                    " id               INTEGER GENERATED ALWAYS AS IDENTITY  PRIMARY KEY," +
                    "  name             CHARACTER VARYING(50)        NOT NULL," +
                    " surname          CHARACTER VARYING(50)        NOT NULL," +
                    " patronymic       CHARACTER VARYING(50)        NOT NULL," +
                    "phone_number     CHARACTER VARYING(15)        NOT NULL," +
                    " birthday         DATE                         NOT NULL," +
                    "employment_date  DATE                         NOT NULL," +
                    " project_id       INTEGER                      NOT NULL," +
                    "developer_level  CHARACTER VARYING(2)         NOT NULL," +
                    "english_level    CHARACTER VARYING(2)         NOT NULL," +
                    "skype            CHARACTER VARYING(50)        NOT NULL," +
                    "feedback_id      INTEGER                      NOT NULL);";
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EmployeeRepository repository;
    private Employee expectedEmployee;
    private long expectedId;

    @Before
    public void init() {
        expectedId = 5L;
        String expectedName = "expected_name_5";

        expectedEmployee = new Employee();
        expectedEmployee.setName("expected_name_5");
        expectedEmployee.setSurname("expected_surname_5");
        expectedEmployee.setPatronymic("expected_patronymic_5");
        expectedEmployee.setPhoneNumber("37525 1111111");
        expectedEmployee.setBirthday(LocalDate.now());
        expectedEmployee.setDateOfEmployment(LocalDate.now());
        expectedEmployee.setProjectId(1L);
        expectedEmployee.setDeveloperLevel(DeveloperLevel.J2);
        expectedEmployee.setEnglishLevel(EnglishLevel.A2);
        expectedEmployee.setSkype("expected_skype_5");
        expectedEmployee.setFeedbackId(2L);

        jdbcTemplate.execute(SQL_DROP_TABLE);
        jdbcTemplate.execute(SQL_CREATE_TABLE);
        try {
            List<String> lines = Files.readAllLines(Paths.get(SCRIPT_PATH), Charset.defaultCharset());
            lines.forEach(jdbcTemplate::execute);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deInit() {
        jdbcTemplate.execute("DELETE FROM employee");
    }

    @Test
    public void add_ShouldReturn_Employee_Test() {
        long expectedId = 6;
        long actualId = repository.create(expectedEmployee).getId();
        Assert.assertEquals(expectedId, actualId);
    }

    @Test
    public void singleQuery_ShouldReturn_Entity_Test() {
        String expectedName = "expected_name_1";
        String actualName = repository.singleQuery(new EmployeeByNameSpecification(expectedName)).get().getName();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void singleQuery_ShouldReturn_Empty_Test() {
        String expectedName = "employee";
        Optional<Employee> optionalEmployee = repository.singleQuery(new EmployeeByNameSpecification(expectedName));
        Assert.assertTrue(optionalEmployee.isEmpty());
    }

    @Test
    public void query_ShouldReturn_List_Test() {
        int expectedSize = 5;
        int actualSize = repository.query(new FindAllSpecification()).size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void remove_ShouldReturn_ExpectedNumber_Test() {
        int expectedSize = 4;
        repository.remove(expectedId);
        int actualSize = repository.query(new FindAllSpecification()).size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void update_ShouldReturn_ExpectedNumber_Test() {
        String expectedName = "update";
        expectedEmployee.setName(expectedName);
        expectedEmployee.setId(expectedId);
        repository.update(expectedEmployee);
        String actualName = repository.update(expectedEmployee).getName();
        Assert.assertEquals(expectedName, actualName);
    }

    @Test
    public void findById_ShouldReturn_OptionalOfEntity_Test() {
        Optional<Employee> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isPresent());
    }

    @Test
    public void findById_ShouldReturn_Empty_Test() {
        long expectedId = 1000;
        Optional<Employee> actualOptional = repository.findById(expectedId);
        Assert.assertTrue(actualOptional.isEmpty());
    }
}




